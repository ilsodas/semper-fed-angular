import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";

import { ProductDataService } from '../../product-data.service';
import { IProduct } from '../product/product.interface';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  product: IProduct | any;

  constructor(private route: ActivatedRoute) {
      this.product = {};
  }

  ngOnInit(): void {
    
    this.product = this.route.snapshot.data['product'];

    console.log('PRODUCT Footer!', this.product)
  }

}
