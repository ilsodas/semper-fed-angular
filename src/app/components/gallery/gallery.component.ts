
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

    @Input() images:any[];

    activeImageSrc: string;
    activeImageTitle: string;
    basePath: string;
    selected: boolean = false;

    constructor() {

        this.images = [];
        this.activeImageSrc = '';
        this.activeImageTitle = '';
        this.basePath = './assets/image';

    }

    ngOnInit(): void {
        this.activeImageSrc = `${this.basePath}/${this.images[0].name}`;
        this.activeImageTitle = this.images[0].title;
    }

    setActiveImage(image: any) : void {
        this.activeImageTitle = image.title;
        this.activeImageSrc = `${this.basePath}/${image.name}`;
        this.selected = true;
    }

}
