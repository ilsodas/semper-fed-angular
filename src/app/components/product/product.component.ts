
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";

import { ProductDataService } from '../../product-data.service';
import {IProduct} from './product.interface';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

    product: IProduct | any;

    constructor(private route: ActivatedRoute) {
        this.product = {};
    }

    ngOnInit(): void {
        this.product = this.route.snapshot.data['product'][0];
    }

}
