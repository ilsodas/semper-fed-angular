
export interface IProduct {
    id: number;
    name: string;
    description: string;
    category: string;
    price: {
        startingAt: string;
        value: number;
    },
    tabContent: [
        {
            id: number;
            title: string;
            content: string;
        }
    ],
    options: [
        {
            id: number;
            title: string;
            content: string;
        }
    ],
    warranty: [
        {
            id: number;
            title: string;
            content: string;
            moreInfo: string;
            price?: number
        }
    ],
    productFeatures: [
        {
            id: number;
            content: string;
        }
    ],
    galleryFeatures: [
        {
            icon: string;
            text: string;
        }
    ],
    gallery: [
        {
            title: string;
            name: string;
        }
    ],
    specifications: [
        {
            title: string;
            content: string;
        }
    ],
    shipping: {
        label: string;
        text: string;
    },
    finance: {
        label: string;
        link: string;
    },
    buyNowText: string;
}