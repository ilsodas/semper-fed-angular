
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

    @Input() data: any;

    activeTab: number;

    constructor() {
        this.activeTab = 0;
    }

    ngOnInit(): void { }

    setActiveTab(tab: number) {
        this.activeTab = tab;
    }

}
