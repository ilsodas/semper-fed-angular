
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ProductDataService {

    private readonly endpoint: string;

    constructor(private http: HttpClient) {
        this.endpoint = 'http://localhost:3000/products';
    }

    getData() {
        return this.http.get(this.endpoint);
    }

}
