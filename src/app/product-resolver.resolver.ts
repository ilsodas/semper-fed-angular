
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';

import { ProductDataService } from './product-data.service';

@Injectable({
    providedIn: 'root'
})

export class ProductResolver implements Resolve<any> {

    constructor(private productDataService: ProductDataService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): any {

        return this.productDataService.getData();

    }

}